import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
 import * as $ from 'jquery';
import { bulmaCarousel } from 'bulma-extensions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.cycleBannerBackgrounds();
    this.setScollListener();

    // initialized new section carousel
    const newsCarousels = bulmaCarousel.attach();
  }

  cycleBannerBackgrounds(): void {
    let index = 0;
    const imageEls = $('.carousel-container .slide'); // Get the images to be cycled.

    setInterval(function () {
      // Get the next index.  If at end, restart to the beginning.
      index = index + 1 < imageEls.length ? index + 1 : 0;

      // Show the next
      imageEls.eq(index).addClass('show');

      // Hide the previous
      imageEls.eq(index - 1).removeClass('show');
    }, 4000);
  }

  setScollListener(): void {
    // set color when scrolling navbar
    $(document).scroll(function () {
      const nav = $('.is-fixed-top');
      nav.toggleClass('scrolled', $(this).scrollTop() > nav.height());
    });
  }

}
